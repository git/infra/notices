---
title: wiki.gentoo.org migration
active: false
type: maintenance
created_at: 2014-08-03 05:53
eta: 2014-08-03 08:00
expire_at: 2014-08-04 08:00
affects: [wiki]
---

The wiki has migrated between hosts. Please report any problems to Bugzilla.
