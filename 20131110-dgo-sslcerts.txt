---
title: SSL certificates for Developer SMTP, IMAP, and POP changed
active: false
type: information
created_at: 2013-11-10 00:00
expire_at: 2013-12-10 10:00
affects: [dgo_mbox, dgo_smtp]
---

The SSL certificates used for the developer E-Mail services expired and have been replaced.

The new fingerprints are as follows:

<table class="table table-bordered">
  <tr>
    <th rowspan="3">POP3/IMAP</th>
    <td>MD5</td>
    <td><tt>F7:4C:8E:C5:06:EA:31:F1:B9:F6:9B:80:4A:92:94:94</tt></td>
  </tr>
  <tr>
    <td>SHA1</td>
    <td><tt>E7:59:C8:CE:9E:93:0D:4A:B2:B0:61:53:02:A3:7B:AA:FB:0F:4F:36</tt></td>
  </tr>
  <tr>
    <td>SHA256</td>
    <td><tt>56:32:81:91:CA:32:78:E6:90:AD:15:17:ED:82:5D:A9:AA:A7:76:5D:88:9D:27:D4:CE:47:9A:DC:6A:16:07:4B</tt></td>
  </tr>
  <tr>
    <th rowspan="3">SMTP</th>
    <td>MD5</td>
    <td><tt>45:57:E7:D8:C2:C5:C5:07:B7:20:48:C9:E8:4D:F1:55</tt></td>
  </tr>
  <tr>
    <td>SHA1</td>
    <td><tt>A5:D9:A4:BD:57:A2:38:78:B4:78:CF:FD:77:42:65:12:37:24:B0:9E</tt></td>
  </tr>
  <tr>
    <td>SHA256</td>
    <td><tt>90:7A:7A:F2:70:FF:78:1B:FB:79:01:06:19:B7:65:64:A0:7B:4B:0D:84:54:17:CA:59:E7:07:E6:41:21:CE:F1</tt></td>
  </tr>
</table>
