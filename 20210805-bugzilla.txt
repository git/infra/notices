---
title: Bugzilla service outage due to Perl upgade
active: false
type: outage
created_at: 2021-08-06 01:15
starts_at: 2021-08-06 01:15
eta: 2021-08-06 04:00
expire_at: 2021-08-28 00:00
affects: [bugzilla]
force_state: maintenance
---

Bugzilla (http://bugs.gentoo.org) is undergoing maintenance for a major Perl
version upgrade.
