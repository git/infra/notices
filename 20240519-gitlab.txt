---
title: gitlab.gentoo.org outage
active: true
type: outage
created_at: 2024-05-20 05:00
affects: [gitlab]
expire_at: 2024-06-01 00:00
---

The Gentoo GitLab instance at `https://gitlab.gentoo.org/` is offline due to
hardware failure.

Please stand by for recovery updates after we establish contact with the
sponsor.

2024-05-21 18:00 UTC: Hardware failure, possible RAID controller issue. Verifying data loss vs recovery potential.

2024-05-23 00:00 UTC: Service has been restored temporarily, but maintenance work will be required in the near future. No data loss experienced.
