---
title: "Maintanence Outage: CI"
active: false
type: maintenance
created_at: 2022-04-19 00:00
starts_at: 2022-04-20 00:00
expire_at: 2022-04-27 00:00
affects: [gentoo_ci, pr_ci]
force_state: maintenance
---
Update: 2022-04-21T05:00 UTC: The migration is rescheduled for 2022-04-23T17:00, taking 4-6 hours.

Update: 2022-04-20T05:40 UTC: other hardware issues have cause this maint to be delayed. New schedule available soon. CI services restored.

CI will have delays of 4-6 hours for the service to be migrated between hosts.
