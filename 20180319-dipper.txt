---
title: Mastermirror / mastersync outage.
active: false
type: maintenance
created_at: 2018-03-19 10:00
eta: 2018-03-24 10:00
expire_at: 2018-03-31 10:00
affects: [rsync, distfiles, blogs, projects]
force_state: maintenance
---

The Gentoo mastermirror has experienced a hardware failure. The Gentoo infrastructure
team has allocated replacement hardware and we are working on replacing it. In the meantime,
rsync and distfile mirrors won't be updated and blogs will be unavailable. Git mirrors
are not affected by the issue. Thank you for your patience.
