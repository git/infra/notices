---
title: In-/Outbound Email (SMTP) maintenance
active: false
type: maintenance
created_at: 2014-01-20 21:50
eta: 2014-01-20 22:50
expire_at: 2014-01-21 10:00
affects: [dgo_smtp]
force_state: maintenance
---

The mailing system on dev.gentoo.org is currently down for maintenance and will
be back soonish.
