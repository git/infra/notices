---
title: "gentoo-x86 VCS migration"
active: false
type: maintenance
created_at: 2015-08-08 18:00
affects: [pgo]
force_state: maintenance
---

As [announced](https://archives.gentoo.org/gentoo-dev/message/11a3e57ec0fac2cd6d7defe5539e916e), we are migrating the version control system of our main package repository.

This can affect the availability of various services during and shortly after the migration. We will update this notice with any extended outages.

#### Update (2015-08-10)

rsync mirrors now feed from the Git repository. rsync mirrors are busier than usual, syncing might require multiple attempts. Manifest files are synced every time, this is not on purpose and will be resolved later today.

#### Update (2015-08-11)

Syncing issues are still being worked on.

Some services depending on the package repository are also still pending to be adapted (packages.gentoo.org, <s>snapshots</s>).

#### Update (2015-08-19)

Snapshots are properly created again, ChangeLogs (current as of the migration time) are restored. We're still working on updating the ChangeLogs, not having Manifests resynced every time, as well as packages.gentoo.org.

#### Update (2015-09-02)

Most services operate as intended again. packages.gentoo.org is mostly up to date, but still lacking Changelogs.

#### Update (2015-11-16)

ChangeLogs are now again part of the rsync data
