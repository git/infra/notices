---
title: Bouncer
active: false
type: outage
created_at: 2020-11-25 22:00
starts_at: 2020-11-25 22:00
eta: 2020-12-01 00:00
expire_at: 2020-12-01 00:00
affects: [bouncer]
force_state: maintenance
---

The hardware hosting bouncer is failing. If you encounter problems, please use mirrorselect to select a mirror that is close to you.
