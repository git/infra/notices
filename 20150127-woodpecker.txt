---
title: dev.gentoo.org Maintenance
active: false
type: maintenance
created_at: 2015-01-27 21:00
expire_at: 2015-01-29 10:00
affects: [dgo_http, dgo_smtp, dgo_mbox, dgo_ssh, forums, bugzilla]
---

<kbd>dev.gentoo.org</kbd> is currently undergoing scheduled maintenance, but we are facing difficulties completing the work.

As such, all services on the machine are still unavailable. The outage also affects in- and outbound email, resulting in bug and forums notifications not being sent.

#### Update (Jan 28, 0800 UTC)

Maintenance is still ongoing, services are being relocated to another machine.

#### Update (Jan 28, 2000 UTC)

Maintenance is complete, the services have been restored in a virtual machine. Due to the volume of queued email, it may take a while for all messages to reach their recipients.
