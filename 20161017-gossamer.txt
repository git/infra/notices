---
title: Sponsor Network Maintenance
active: false
type: maintenance
created_at: 2016-10-17 21:00
eta: 2016-10-28 06:00
expire_at: 2016-10-28 12:00
affects: [wiki, bugzilla, forums]
---

The sponsor where <kbd>wiki.gentoo.org</kbd>, <kbd>bugs.gentoo.org</kbd> &amp;
<kbd>forums.gentoo.org</kbd> will be conducting some scheduled network
maintenance in the window of 2016/10/28 04:00-06:00 UTC. 

While our sponsors strive to reduce impact, connectivity may be intermittant
for up to 5 minutes during the maintenance window.
